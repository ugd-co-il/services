import { Component, OnInit } from '@angular/core';
import { SService } from '../s.service';
import { TService } from '../t.service';

@Component({
  selector: 'app-b',
  templateUrl: './b.component.html',
  styleUrls: ['./b.component.css'],
  providers: [SService]
})
export class BComponent implements OnInit {
  private sServiceData = '';
  private tServiceData = '';

  constructor(private sService:SService, private tService:TService) { }

  setTServiceData(newData) {
    this.tService.setData(newData);
  }

  setSServiceData(newData) {
    this.sService.setData(newData);
  }

  ngOnInit() {
    this.sService.data.subscribe((newData) => {
      this.sServiceData = newData;
    });

    this.tService.data.subscribe((newData) => {
      this.tServiceData = newData;
    });
  }

}
