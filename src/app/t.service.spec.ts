import { TestBed, inject } from '@angular/core/testing';

import { TService } from './t.service';

describe('TService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TService]
    });
  });

  it('should be created', inject([TService], (service: TService) => {
    expect(service).toBeTruthy();
  }));
});
