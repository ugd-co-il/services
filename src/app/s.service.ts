import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import 'rxjs';

@Injectable()
export class SService {
  public data:Subject<string> = new Subject<string>();

  constructor() {}

  setData(newData:string) {
    this.data.next(newData);
  }

}
