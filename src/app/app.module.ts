import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { TService } from './t.service';

import { AppComponent } from './app.component';
import { AComponent } from './a/a.component';
import { BComponent } from './b/b.component';


@NgModule({
  declarations: [
    AppComponent,
    AComponent,
    BComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [TService],
  bootstrap: [AppComponent]
})
export class AppModule { }
